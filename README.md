# Transcrobes Utils

This repo houses various group-wide utilities that are shared.

Some of them may turn out to be useful for others, and will be spun out into their own projects

# Included tools

## release.py
Based heavily on https://gitlab.com/alelec/gitlab-release/ but this script uses the python-gitlab library instead of calling the API with requests directly. The idea is to support new Gitlab features as soon as they come out (like Release Links in Gitlab 11.8).


## Developer Certificate of Origin
Please sign all your commits by using `git -s`. In the context of this project this means that you are signing the document available at https://developercertificate.org/. This basically certifies that you have the right to make the contributions you are making, given this project's licence. You retain copyright over all your contributions - this sign-off does NOT give others any special rights over your copyrighted material in addition to the project's licence.

## Contributing
See [the website](https://transcrob.es/page/contribute) for more information. Please also take a look at our [code of conduct](https://transcrob.es/page/code_of_conduct) (or CODE\_OF\_CONDUCT.md in this repo).
