
# FIXME: apparently this can fail... so should check to make sure and re-run if nec
# can use something like this:
# Get-NetIPInterface | select ifIndex,InterfaceAlias,AddressFamily,ConnectionState,Forwarding | Sort-Object -Property IfIndex | Format-Table

Set-NetIPInterface -ifindex (Get-NetIPInterface -InterfaceAlias "vEthernet (Default Switch)" | select ifIndex | Get-Unique).ifIndex -Forwarding Enabled

Set-NetIPInterface -ifindex (Get-NetIPInterface -InterfaceAlias "vEthernet (WSL)" | select ifIndex | Get-Unique).ifIndex -Forwarding Enabled
