#!/usr/bin/bash

# Do once per host, harmless to execute many times
# on proper ubuntu
# sudo snap install multipass --classic
# sudo snap install helm --classic
# sudo snap install kubectl --classic

# on WSL2
# or install manually on Windows 10 for WSL2 using their installer, because snapd requires systemd
# FIXME: xenial appears to work on focal and their is no focal yet
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
curl https://helm.baltorepo.com/organization/signing.asc | sudo apt-key add -
sudo apt install -y apt-transport-https
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt update
sudo apt install helm

mkdir -p ~/.kube/

# multipass delete mk8s && multipass purge

multipass.exe launch focal --name mk8s --mem 4G --disk 40G --cpus 2

multipass.exe exec mk8s -- sudo snap install microk8s --classic --channel=1.18/stable

multipass.exe exec mk8s -- sudo usermod -a -G microk8s ubuntu

multipass.exe exec mk8s -- /snap/bin/microk8s.enable dns storage

echo 'waiting for a bit so that storage initialises (im too lazy to check the storage pod has initialised)'
seconds=10; date1=$((`date +%s` + $seconds));
while [ "$date1" -ge `date +%s` ]; do
  echo -ne "$(date -u --date @$(($date1 - `date +%s` )) +%H:%M:%S)\r";
done

# TODO: don't use the default config file, as we are using aliases we might as well use a file that we
# pass to the alias
multipass.exe exec mk8s -- /snap/bin/microk8s.config > ~/.kube/config

# multipass.exe mount /mnt/c/User/anton/dev mk8s:/home/ubuntu/dev

# install cert-manager
kubectl create namespace cert-manager

helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v0.15.1 --set installCRDs=true

# this is pretty much required because the IP changes, so we need a domain name, and the config command
# doesn't know about this domain. Maybe it is possible to make microk8s.config accept an arbitrary domain name?
# echo "alias k='/usr/bin/kubectl --insecure-skip-tls-verify '" > ~/conf/.mk8s.bash
echo "alias k='bash ~/dev/tc/utils/mk8s_comm.sh /usr/bin/kubectl'" > ~/conf/.mk8s.bash
echo "alias h='bash ~/dev/tc/utils/mk8s_comm.sh /usr/sbin/helm'" >> ~/conf/.mk8s.bash

# this is just handy
echo "alias mpem='/mnt/c/Program\ Files/Multipass/bin/multipass.exe exec mk8s -- '" >> ~/conf/.mk8s.bash

source ~/conf/.mk8s.bash

# TODO:
# replace ip in ~/.kube/config with mk8s.mshome.net

