#!/usr/bin/bash

cur_ip=$(multipass.exe list | grep mk8s | awk '{print $3}')

if ! grep -q "$cur_ip" ~/.kube/config; then
    echo "Updating the kubectl ip to $cur_ip"
    multipass.exe exec mk8s -- /snap/bin/microk8s.config > ~/.kube/config
fi

bin=$1
shift

eval $bin "$@"
